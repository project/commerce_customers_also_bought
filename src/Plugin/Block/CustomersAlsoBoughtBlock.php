<?php

namespace Drupal\commerce_customers_also_bought\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Cache\Cache;

/**
 * Customers also bought block.
 *
 * @Block(
 *   id = "commerce_customers_also_bought_block",
 *   admin_label = @Translation("Customers also bought"),
 *
 * )
 */
class CustomersAlsoBoughtBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected $entityDisplayRepository;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The route match interface.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * Constructs a CustomersAlsoBoughtBlock class.
   */
  public function __construct(array $configuration,
                              $plugin_id,
                              $plugin_definition,
                              EntityDisplayRepositoryInterface $entity_display_repository,
                              EntityTypeManagerInterface $entity_type_manager,
                              RouteMatchInterface $route_match,
                              Connection $connection) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->entityDisplayRepository = $entity_display_repository;
    $this->entityTypeManager = $entity_type_manager;
    $this->routeMatch = $route_match;
    $this->connection = $connection;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'max_age' => 172800,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_display.repository'),
      $container->get('entity_type.manager'),
      $container->get('current_route_match'),
      $container->get('database')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $route_name = $this->routeMatch->getRouteName();
    $product = $this->routeMatch->getParameter('commerce_product');
    $product_id = NULL;

    $number_of_products = $this->configuration['number_of_products'];
    $number_of_items = $number_of_products * 10;

    if ($product) {
      $product_id = $product->id();
    }

    $query = $this->connection->select('commerce_order', 'co')
      ->fields('co', ['order_id']);

    $query->leftJoin('commerce_order_item', 'coi', 'co.order_id=coi.order_id');
    $query->leftJoin('commerce_product_variation_field_data', 'pvfd', 'coi.purchased_entity=pvfd.variation_id');
    $query->condition('co.state', 'completed');

    if ($product_id) {
      $query->condition('pvfd.product_id', $product_id);
    }

    $query->groupBy('co.order_id');
    $query->orderBy('co.created', 'DESC');

    $order_ids = $query->range(0, $number_of_items)
      ->execute()
      ->fetchCol();

    $product_ids = [];

    foreach ($order_ids as $order_id) {
      /** @var \Drupal\commerce_order\Entity\Order $order */
      $order = $this->entityTypeManager->getStorage('commerce_order')
        ->load($order_id);

      if ($order) {
        foreach ($order->getItems() as $order_item) {
          /** @var \Drupal\commerce_product\Entity\ProductVariation $variation */
          $variation = $order_item->getPurchasedEntity();

          if ($variation) {
            $variation_product = $variation->getProduct();

            if ($variation_product && $variation_product->isPublished()) {
              if ($route_name == 'entity.commerce_product.canonical') {
                if ($product_id != $variation_product->id()) {
                  $product_ids[] = $variation_product->id();
                }
              }
              else {
                $product_ids[] = $variation_product->id();
              }
            }
          }
        }
      }
    }

    if ($product_ids) {
      return [
        '#theme' => 'customers_also_bought',
        '#products' => $this->getRenderedProducts($product_ids, $number_of_products, $this->configuration['view_mode']),
      ];
    }
    else {
      return [];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    $view_modes = $this->entityDisplayRepository->getViewModes('commerce_product');
    $modes = [];
    foreach ($view_modes as $key => $view_mode) {
      $modes[$key] = $view_mode['label'];
    }

    $form['view_mode'] = [
      '#type' => 'select',
      '#title' => $this->t('Select view mode'),
      '#options' => $modes,
      '#default_value' => isset($config['view_mode']) ? $config['view_mode'] : '',
    ];

    $form['number_of_products'] = [
      '#type' => 'number',
      '#title' => $this->t('Number of products to show'),
      '#required' => TRUE,
      '#default_value' => isset($config['number_of_products']) ? $config['number_of_products'] : '',
    ];

    $form['max_age'] = [
      '#title' => $this->t('Cache'),
      '#type' => 'select',
      '#options' => [
        '0' => 'No Caching',
        '1800' => '30 Minutes',
        '3600' => '1 Hour',
        '21600' => '6 Hours',
        '43200' => '12 Hours',
        '86400' => '1 Day',
        '172800' => '2 Days',
        '432000' => '5 Days',
        '604800' => '1 Week',
        '-1' => 'Permanent',
      ],
      '#default_value' => $this->configuration['max_age'],
      '#description' => $this->t('Set the max age the block is allowed to be cached for.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);

    $values = $form_state->getValues();
    $this->configuration['number_of_products'] = $values['number_of_products'];
    $this->configuration['view_mode'] = $values['view_mode'];
    $this->configuration['max_age'] = $values['max_age'];
  }

  /**
   * Gets random products and renders them in the selected view mode.
   *
   * @param array $product_ids
   *   An array of product ids.
   * @param integer $number_of_products
   *   The number of products that should be returned.
   * @param string $view_mode
   *   The view mode that is used for rendering the products.
   *
   * @return array $products
   *   An array of rendered products.
   */
  public function getRenderedProducts($product_ids, $number_of_products, $view_mode) {
    $products = [];
    $random_products = $this->getRandomProducts($product_ids, $number_of_products);

    foreach ($random_products as $product) {
      $products[] = $this->entityTypeManager->getViewBuilder('commerce_product')
        ->view($product, $view_mode);
    }

    return $products;
  }

  /**
   * Gets random products.
   *
   * @param array $product_ids
   *   An array of product ids.
   * @param integer $number_of_products
   *   The number of products that should be returned.
   */
  public function getRandomProducts($product_ids, $number_of_products) {
    $random_products = [];
    $unique_product_ids = array_values(array_unique($product_ids));

    while (count($random_products) < $number_of_products) {
      $rand_key = mt_rand(0, count($unique_product_ids) - 1);
      $product_id = $unique_product_ids[$rand_key];

      $product = $this->entityTypeManager->getStorage('commerce_product')
        ->load($product_id);

      if ($product) {
        $random_products[] = $product;
      }

      array_splice($unique_product_ids, array_search($product_id, $unique_product_ids), 1);

      if (empty($unique_product_ids)) {
        break;
      }
    }

    return $random_products;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return Cache::mergeContexts(parent::getCacheContexts(), ['url']);
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    return Cache::mergeTags(parent::getCacheTags(), ['commerce_product_list']);
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return Cache::mergeMaxAges(parent::getCacheMaxAge(), $this->configuration['max_age']);
  }

}
